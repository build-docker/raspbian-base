ifndef CI_REGISTRY
CI_REGISTRY := registry.icinga.com
endif

ifeq ($(CI_PROJECT_PATH),)
CI_PROJECT_PATH := build-docker/raspbian-base
endif
ifeq ($(CI_COMMIT_REF_NAME),)
CI_COMMIT_REF_NAME := $(shell git rev-parse --abbrev-ref HEAD)
endif

IMAGE := $(CI_PROJECT_PATH)/${CI_COMMIT_REF_NAME}

ifneq ($(CI_REGISTRY),)
IMAGE := $(CI_REGISTRY)/$(IMAGE)
endif

QEMU_VERSION = v4.0.0-5
QEMU_STATIC = https://github.com/multiarch/qemu-user-static/releases/download/$(QEMU_VERSION)/qemu-arm-static.tar.gz
QEMU_CHECKSUM = 3fc9536f712b02c5c83cb45b4deba5c48dc87b3a48795ca4189da08ab6f25ac54495bc56e5e1914f595dc2ca68dcd04977264e9f19d5754e88ed3a6dea68ac12

.PHONY: all tarball clean

all: build

tarball: rootfs.tar.xz
rootfs.tar.xz:
	TARBALL="$@.tmp" ./make-image.sh
	mv "$@.tmp" "$@"

qemu-arm-static:
	wget "$(QEMU_STATIC)" -O $@.tar.gz
	echo "$(QEMU_CHECKSUM)  $@.tar.gz" | sha512sum -c - || (sha512sum $@.tar.gz; false)
	tar xf $@.tar.gz
	rm -f $@.tar.gz

build: qemu-arm-static tarball
	docker build --tag "$(IMAGE)" .

push:
	docker push "$(IMAGE)"

clean:
	rm -f *.tar*
	if (docker inspect --type image "$(IMAGE)" >/dev/null 2>&1); then docker rmi "$(IMAGE)"; fi

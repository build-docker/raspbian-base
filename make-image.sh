#!/bin/sh

DIST=bullseye
GPGKEY=A0DA38D0D76E8B5D638872819165938D90FDDD2E

if [ -z ${CI_REGISTRY+x} ]; then
  CI_REGISTRY=registry.icinga.com
fi
if [ -z "$CI_PROJECT_PATH" ]; then
  CI_PROJECT_PATH=build-docker/raspbian-base
fi

IMAGE="${CI_PROJECT_PATH}/debootstrap"

if [ -n "${CI_REGISTRY}" ]; then
  IMAGE="${CI_REGISTRY}/${IMAGE}"
fi

opts=
if [ -t 1 ] && [ -t 0 ]; then
  opts=-t
fi

docker run -i $opts --rm \
  --privileged \
  -e DIST="$DIST" \
  -e GPGKEY="$GPGKEY" \
  -e "TARBALL=$TARBALL" \
  -v "$(pwd):/work" -w "/work" \
  "$IMAGE" "$@"
